from django.conf.urls import *
from django.contrib.auth.decorators import login_required
#from django.views.generic.simple import direct_to_template
from django.views.generic import TemplateView

from invitation.views import RegistrationView, InviteView

from registration.forms import RegistrationFormTermsOfService
# from invitation.views import invite, invited, register


urlpatterns = patterns('',
    url(r'^invite/complete/$',
                TemplateView.as_view(
                template_name = 'invitation/invitation_complete.html'),
                name='invitation_complete'),
    url(r'^invite/$',
                login_required(InviteView.as_view()),
                name='invitation_invite'),
    # url(r'^invited/(?P<invitation_key>\w+)/$', 
    #             invited,
    #             name='invitation_invited'),
    url(r'^register/$',
        RegistrationView.as_view(),
        name='registration_register'),
)

