from django.conf import settings
#from django.views.generic.simple import direct_to_template
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.generic.edit import FormView

# from registration.views import register as registration_register
from registration.backends.default.views import RegistrationView as RegistrationViewOriginal
from registration.forms import RegistrationForm

from invitation.models import InvitationKey
from invitation.forms import InvitationKeyForm

is_key_valid = InvitationKey.objects.is_key_valid

# TODO: move the authorization control to a dedicated decorator

class RegistrationView(RegistrationViewOriginal):
    def invited(request, invitation_key=None):
        if hasattr(settings, 'INVITE_MODE') and settings.INVITE_MODE:
            if invitation_key and is_key_valid(invitation_key):
                template = 'invitation/invited.html'
            else:
                template = 'invitation/wrong_invitation_key.html'
            return render(request, template, {'invitation_key': invitation_key})
        else:
            return HttpResponseRedirect(reverse('registration_register'))

    def register(request, success_url=None,
                form_class=RegistrationForm, profile_callback=None,
                template_name='registration/registration_form.html',
                extra_context=None):
        if hasattr(settings, 'INVITE_MODE') and settings.INVITE_MODE:
            if 'invitation_key' in request.REQUEST \
                and is_key_valid(request.REQUEST['invitation_key']):
                if extra_context is None:
                    extra_context = {'invitation_key': request.REQUEST['invitation_key']}
                else:
                    extra_context.update({'invitation_key': invitation_key})
                return super(RegistrationView, self).register(request, success_url, form_class, 
                            profile_callback, template_name, extra_context)
            else:
                return render(request, 'invitation/wrong_invitation_key.html')
        else:
            return super(RegistrationView, self).register(request,
                self.success_url, self.form_class, 
                profile_callback, template_name, extra_context)

class InviteView(FormView):
    form_class = InvitationKeyForm
    template_name = 'invitation/invitation_form.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {
            'form': form,
            'remaining_invitations': InvitationKey.objects.remaining_invitations_for_user(request.user),})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST, files=request.FILES)
        if form.is_valid():
            try:
                # for_user = User.objects.get(email=form.cleaned_data["email"])
                for_email = form.cleaned_data["email"]
            except User.DoesNotExist:
                for_email  = None
            invitation = InvitationKey.objects.create_invitation(request.user,
                for_email=for_email)
            invitation.send_to(form.cleaned_data["email"])
            # success_url needs to be dynamically generated here; setting a
            # a default value using reverse() will cause circular-import
            # problems with the default URLConf for this application, which
            # imports this file.
            return HttpResponseRedirect(self.success_url or reverse('invitation_complete'))
        print "invalid"
        return render(request, self.template_name, {
            'form': form,
            'remaining_invitations': InvitationKey.objects.remaining_invitations_for_user(request.user),})
    
# invite = login_required(invite)

